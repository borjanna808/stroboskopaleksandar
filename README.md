# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://Aleksandar97@bitbucket.org/Aleksandar97/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/Aleksandar97/stroboskop/commits/4340635307c895398868de64e535c628833d3f25

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Aleksandar97/stroboskop/commits/67a22b21c249cf72006c16793da7d875a4cdfa0f

Naloga 6.3.2:
https://bitbucket.org/Aleksandar97/stroboskop/commits/3ae1faadb80fe5de98e8bc529a1e92585ff79d16

Naloga 6.3.3:
https://bitbucket.org/Aleksandar97/stroboskop/commits/b6754be6e6b77f87a12de62b0a0533aafaf1ba91?at=%3CGitBranch%20refs/heads/izgled%3E

Naloga 6.3.4:
https://bitbucket.org/Aleksandar97/stroboskop/commits/017444b23002210364c464d88e59bad2bbf6dbee?at=master

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Aleksandar97/stroboskop/commits/f7fde92c3461817ae53a3f95eb58670dc3456184

Naloga 6.4.2:
https://bitbucket.org/Aleksandar97/stroboskop/commits/ba7ddbe516bd8c2050c65dfd016834c5acd18990

Naloga 6.4.3:
https://bitbucket.org/Aleksandar97/stroboskop/commits/3d1c20e8f74623d1aa9aabbc011e9bd6b75ed2b9

Naloga 6.4.4:
https://bitbucket.org/Aleksandar97/stroboskop/commits/17fbef08a06b715edc60807f77c099c4c0528664